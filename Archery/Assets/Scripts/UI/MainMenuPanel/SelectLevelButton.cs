﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelButton : MonoBehaviour
{
    [SerializeField] LevelView LevelViewPrefab;

    public delegate void PlayerSelectLevelDel(LevelView levelView);
    public static event PlayerSelectLevelDel PlayerSelectLevelEvent;

    private void Awake()
    {
        GetComponentInChildren<Text>().text = LevelViewPrefab.name;
        GetComponent<Button>().onClick.AddListener(PlayerPushButton);
    }

    private void PlayerPushButton()
    {
        if(PlayerSelectLevelEvent != null)
        {
            PlayerSelectLevelEvent(LevelViewPrefab);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    [SerializeField] private MainMenuPanel MainMenuPanelPrefab;

    private MainMenuPanel MainMenuPanel { get; set; }


    private void Awake()
    {
        Init();   
    }
    private void OnDestroy()
    {
        SelectLevelButton.PlayerSelectLevelEvent -= PlayerSelectLevel;
    }


    private void Init()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

        SelectLevelButton.PlayerSelectLevelEvent += PlayerSelectLevel;

        MainMenuPanel = Instantiate(MainMenuPanelPrefab, transform);
    }

    private void PlayerSelectLevel(LevelView level)
    {
        MainMenuPanel.Hide();
    }
}

﻿public enum EnumUnitState
{    
    Prepare,
    Move,
    Stay,
    Aim,
    Shoot,
}

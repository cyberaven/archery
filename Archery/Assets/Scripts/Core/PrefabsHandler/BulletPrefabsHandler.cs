﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPrefabsHandler : MonoBehaviour
{
    public static BulletPrefabsHandler Instance;

    [SerializeField] private BulletView standartBulletView;
       
    public BulletView StandartBulletView { get => standartBulletView; }

    private void Awake()
    {
        Instance = this;
    }
}

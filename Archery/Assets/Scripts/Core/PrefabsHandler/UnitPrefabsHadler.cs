﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitPrefabsHadler : MonoBehaviour
{
    public static UnitPrefabsHadler Instance;
    
    [SerializeField] private UnitView standartPlayerView;
    [SerializeField] private UnitView walkMonsterView;
    [SerializeField] private UnitView runMonsterView;
    [SerializeField] private UnitView flyMonsterView;

    public UnitView StandartPlayerView { get => standartPlayerView; }
    public UnitView WalkMonsterView { get => walkMonsterView; }
    public UnitView RunMonsterView { get => runMonsterView; }
    public UnitView FlyMonsterView { get => flyMonsterView; }

    private void Awake()
    {
        Instance = this;
    }
}

﻿public interface IUnitLogicSystem
{
    int ChangeBehaviourCooldown{ get; set; }
    void Start();
}
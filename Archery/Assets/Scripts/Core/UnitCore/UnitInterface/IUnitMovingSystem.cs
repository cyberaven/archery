﻿public interface IUnitMovingSystem
{
    float MoveSpeed { get; set; }
}
﻿public interface IUnitAimSystem
{
    IUnit GetTarget();
}
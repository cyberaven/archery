﻿public interface IUnit
{
    UnitView UnitView { get; set; }
    EnumUnitType UnitType { get; set; }
    EnumUnitMovementType UnitMovementType { get; set; }
    IUnitStateSystem UnitState { get; set; }
    IUnitMovingSystem UnitMovingSystem { get; set; }
    IUnitAimSystem UnitAimSystem { get; set; }
    IUnitShootingSystem UnitShootingSystem { get; set; }
    IUnitLogicSystem UnitLogicSystem { get; set; }
}

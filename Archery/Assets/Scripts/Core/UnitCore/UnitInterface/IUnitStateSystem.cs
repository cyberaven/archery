﻿public interface IUnitStateSystem : IObservable
{
    void SetUnitState(EnumUnitState unitState);
    EnumUnitState GetUnitState();
}

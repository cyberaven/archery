﻿using UnityEngine;
using System.Collections;

public class UnitFlyMovingSystem : IUnitMovingSystem, IObserver
{
    private IUnit Unit { get; set; }


    private float moveSpeed = 0.005f;
    public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }

    private bool MoveRandom = false;
    private bool MoveToPlayer = false;
    private Vector3 MoveTarget { get; set; }
   
    float MoveProgress = 0f;


    public UnitFlyMovingSystem(IUnit unit)
    {
        Unit = unit;       
        Unit.UnitState.AddObserver(this);
    }


    public void Update()
    {
        if (Unit.UnitState.GetUnitState() == EnumUnitState.Move)
        {
            SelectMoveBehaviour();
            MoveTarget = SelectTarget();
            UnityMonobehHelper.Instance.CreateNewFixUpdate(Move);            
        }
    }
    private void SelectMoveBehaviour()
    {
        int r = RandomForManyThreads.GetRandN(0, 2);
        if (r == 0)
        {
            MoveRandom = true;
            MoveToPlayer = false;
        }
        else
        {
            MoveRandom = false;
            MoveToPlayer = true;
        }
    }
    private Vector3 SelectTarget()
    {
        Vector3 result = Vector3.zero;


        if (MoveRandom == true)
        {
            float x = RandomForManyThreads.GetRandN(-3, 3);//размеры поля
            float y = Unit.UnitView.transform.position.y;
            float z = RandomForManyThreads.GetRandN(-8, 8);

            result = new Vector3(x, y, z);
        }
        if (MoveToPlayer == true)
        {
            Player p = UnitFactory.Instance.GetPlayer() as Player;
            result = p.UnitView.transform.position;
        }
        return result;
    }

    private void Move()
    {        
        RotateToMovePosition();

        Unit.UnitView.transform.position = Vector3.Lerp(Unit.UnitView.transform.position, MoveTarget, MoveProgress);
        MoveProgress += MoveSpeed;  
        
        CheckPosition();
    }
    private void RotateToMovePosition()
    {
        Unit.UnitView.transform.LookAt(MoveTarget);
    }
    private void CheckPosition()
    {
        if(Vector3.Distance(Unit.UnitView.transform.position, MoveTarget) < 0.1f)
        {
            MoveProgress = 0f;
            UnityMonobehHelper.Instance.StopFixUpdate(Move);
            Unit.UnitState.SetUnitState(EnumUnitState.Stay);
        }       
    }

}

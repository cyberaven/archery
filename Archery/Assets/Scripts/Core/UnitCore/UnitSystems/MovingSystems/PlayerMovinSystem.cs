﻿using UnityEngine;
using System.Collections;

public class PlayerMovinSystem : IUnitMovingSystem
{
    private IUnit Unit;
    
    private float moveSpeed = 10;
    public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }    
    
    private float rotateSpeed = 5;
    public float RotateSpeed { get => rotateSpeed; set => rotateSpeed = value; }   

    public PlayerMovinSystem(IUnit unit)
    {
        Unit = unit;      

        UnityMonobehHelper.PlayerPressWEvent += PlayerPressW;
        UnityMonobehHelper.PlayerPressSEvent += PlayerPressS;
        UnityMonobehHelper.PlayerPressAEvent += PlayerPressA;
        UnityMonobehHelper.PlayerPressDEvent += PlayerPressD;

        UnityMonobehHelper.PlayerUPBtnEvent += PlayerUPBtn;
    }

    private void PlayerPressW()
    {
        MoveHorizontalVertical();          
    }
    private void PlayerPressS()
    {
        MoveHorizontalVertical();
    }
    private void PlayerPressA()
    {
        Unit.UnitState.SetUnitState(EnumUnitState.Move);

        Unit.UnitView.transform.Rotate(Vector3.up, -RotateSpeed);        
    }
    private void PlayerPressD()
    {
        Unit.UnitState.SetUnitState(EnumUnitState.Move);

        Unit.UnitView.transform.Rotate(Vector3.up, RotateSpeed);
    }
    private void MoveHorizontalVertical()
    {
        Unit.UnitState.SetUnitState(EnumUnitState.Move);

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");        
        
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        Unit.UnitView.transform.Translate(movement * MoveSpeed * Time.fixedDeltaTime);
        
    }
    private void CheckPosition(Vector3 endPos)
    {        
        if (Vector3.Distance(Unit.UnitView.transform.position, endPos) < 0.1f)
        {                     
            Unit.UnitState.SetUnitState(EnumUnitState.Stay);
        }
    }
    private void PlayerUPBtn()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        if (-1f < moveHorizontal && moveHorizontal < 1f && -1f < moveVertical && moveVertical < 1f)
        {
            Unit.UnitState.SetUnitState(EnumUnitState.Aim);
        }        
    }
}

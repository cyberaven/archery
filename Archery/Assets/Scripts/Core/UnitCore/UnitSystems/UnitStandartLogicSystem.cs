﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class UnitStandartLogicSystem : IUnitLogicSystem
{
    private int changeBehaviourCooldown = 5;
    public int ChangeBehaviourCooldown { get => changeBehaviourCooldown; set => changeBehaviourCooldown = value; }

    private IUnit Unit { get; set; }   

    public UnitStandartLogicSystem(IUnit unit)
    {
        Unit = unit;
        Unit.UnitState.SetUnitState(EnumUnitState.Prepare);
    }

    public void Start()
    {
        StartBehaviourTimer(ChangeBehaviourCooldown);       
    }
    private void StartBehaviourTimer(int mSec)
    {
        UnityMonobehHelper.Instance.CreateNewTimer(ChangeBehaviour, ChangeBehaviourCooldown);        
    }
    private void ChangeBehaviour()
    {                
        int r = RandomForManyThreads.GetRandN(0, 2);
        if (Unit.UnitState.GetUnitState() != EnumUnitState.Move)
        {
            if (r == 0)
            {
                Unit.UnitState.SetUnitState(EnumUnitState.Stay);
            }
            else
            {
                Unit.UnitState.SetUnitState(EnumUnitState.Move);
            }
        }
        if (Unit.UnitState.GetUnitState() == EnumUnitState.Stay)
        {
            Unit.UnitState.SetUnitState(EnumUnitState.Aim);
        }
            Debug.Log(Unit + " Change Unit State " + Unit.UnitState.GetUnitState());
    }  

}

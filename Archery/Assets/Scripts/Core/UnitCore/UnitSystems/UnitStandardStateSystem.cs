﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitStandardStateSystem : IUnitStateSystem
{
    private EnumUnitState UnitState;
    private IUnit Unit { get; set; }

    private List<IObserver> Observers = new List<IObserver>();
    
    public UnitStandardStateSystem(IUnit unit)
    {
        Unit = unit;
        UnitState = EnumUnitState.Stay;
    }

    public void SetUnitState(EnumUnitState unitState)
    {
        UnitState = unitState;     
        NotifyObservers();
    }
    public EnumUnitState GetUnitState()
    {
        return UnitState;
    }


    public void AddObserver(IObserver o)
    {
        Observers.Add(o);
    }
    public void DelObserver(IObserver o)
    {
        Observers.Remove(o);
    }
    public void NotifyObservers()
    {
        foreach (IObserver observer in Observers)
        {
            observer.Update();
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class UnitStandartShootingSystem : IUnitShootingSystem, IObserver
{
    private IUnit Unit { get; set; }
    
    private int ShootingSpeed = 1;

    private IUnit Target { get; set; }

    public UnitStandartShootingSystem(IUnit unit)
    {
        Unit = unit;
        Unit.UnitState.AddObserver(this);
    }

    public void Update()
    {
        if(Unit.UnitState.GetUnitState() == EnumUnitState.Shoot)
        {
            Target = Unit.UnitAimSystem.GetTarget();
            UnityMonobehHelper.Instance.CreateNewTimer(Shoot, ShootingSpeed);            
        }
        else
        {
            UnityMonobehHelper.Instance.StopTimer(Shoot);
        }
    }

    private void Shoot()
    {
        RotateAtTarget();
        CreateBullet();        
        Debug.Log(Unit + " pew pew.");
    }
    private void RotateAtTarget()
    {
        Unit.UnitView.transform.LookAt(Target.UnitView.transform.position);
    }
    private void CreateBullet()
    {
        BulletView b = UnityEngine.Object.Instantiate(BulletPrefabsHandler.Instance.StandartBulletView);
        b.transform.position = Unit.UnitView.transform.position;
        b.transform.Translate(Unit.UnitView.transform.forward * 1.1f);
        b.Target = Target.UnitView.transform.position;
        b.Go();
    }
}

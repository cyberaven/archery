﻿using UnityEngine;
using System.Collections;

public class UnitStandartAimSystem : IUnitAimSystem, IObserver
{
    private IUnit Unit { get; set; }

    private IUnit Target { get; set; }

    public UnitStandartAimSystem(IUnit unit)
    {
        Unit = unit;
        Unit.UnitState.AddObserver(this);
    }
    public void Update()
    {
        if(Unit.UnitState.GetUnitState() == EnumUnitState.Aim)
        {
            Target = UnitFactory.Instance.GetNearEnemy(Unit);
            Unit.UnitState.SetUnitState(EnumUnitState.Shoot);
        }
    }
    public IUnit GetTarget()
    {
        return Target;
    }
}

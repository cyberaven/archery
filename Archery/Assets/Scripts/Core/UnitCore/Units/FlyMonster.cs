﻿using UnityEngine;

public class FlyMonster : AbstractUnit
{
    public FlyMonster(UnitStartPosition unitStartPosition, UnitView unitView)
    {
        UnitView = Object.Instantiate(unitView);
        UnitView.transform.position = unitStartPosition.transform.position;

        UnitType = EnumUnitType.Monster;
        UnitMovementType = EnumUnitMovementType.Fly;
        UnitState = new UnitStandardStateSystem(this);
        UnitMovingSystem = new UnitFlyMovingSystem(this);
        UnitAimSystem = new UnitStandartAimSystem(this);
        UnitShootingSystem = new UnitStandartShootingSystem(this);
        UnitLogicSystem = new UnitStandartLogicSystem(this);
    }
}

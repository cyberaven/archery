﻿using UnityEngine;

public class WalkMonster : AbstractUnit
{   

    public WalkMonster(UnitStartPosition unitStartPosition, UnitView unitView)
    {
        UnitView = Object.Instantiate(unitView);
        UnitView.transform.position = unitStartPosition.transform.position;

        UnitType = EnumUnitType.Monster;
        UnitMovementType = EnumUnitMovementType.Walk;        
        UnitState = new UnitStandardStateSystem(this);
        UnitMovingSystem = new UnitWalkMovingSystem(this);
        UnitAimSystem = new UnitStandartAimSystem(this);
        UnitShootingSystem = new UnitStandartShootingSystem(this);
        UnitLogicSystem = new UnitStandartLogicSystem(this);
    }
}

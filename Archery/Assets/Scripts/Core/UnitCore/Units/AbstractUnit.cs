﻿public abstract class AbstractUnit : IUnit
{
    public UnitView UnitView { get; set; }
    public EnumUnitType UnitType { get; set; }
    public EnumUnitMovementType UnitMovementType { get; set; }
    public IUnitStateSystem UnitState { get; set; }
    public IUnitMovingSystem UnitMovingSystem { get; set; }
    public IUnitAimSystem UnitAimSystem { get; set; }
    public IUnitShootingSystem UnitShootingSystem { get; set; }
    public IUnitLogicSystem UnitLogicSystem { get; set; }

}

﻿using UnityEngine;

public class RunMonster : AbstractUnit
{
    public RunMonster(UnitStartPosition unitStartPosition, UnitView unitView)
    {
        UnitView = Object.Instantiate(unitView);
        UnitView.transform.position = unitStartPosition.transform.position;

        UnitType = EnumUnitType.Monster;
        UnitMovementType = EnumUnitMovementType.Run;
        UnitState = new UnitStandardStateSystem(this);
        UnitMovingSystem = new UnitRunMovingSystem(this);
        UnitAimSystem = new UnitStandartAimSystem(this);
        UnitShootingSystem = new UnitStandartShootingSystem(this);
        UnitLogicSystem = new UnitStandartLogicSystem(this);
    }
}

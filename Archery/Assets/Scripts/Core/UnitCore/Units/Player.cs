﻿using UnityEngine;

public class Player : AbstractUnit
{
    public Player(UnitStartPosition unitStartPosition, UnitView unitView)
    {
        UnitView = Object.Instantiate(unitView);
        UnitView.transform.position = unitStartPosition.transform.position;

        UnitType = EnumUnitType.Player;
        UnitState = new UnitStandardStateSystem(this);
        UnitMovingSystem = new PlayerMovinSystem(this);
        UnitAimSystem = new UnitStandartAimSystem(this);
        UnitShootingSystem = new UnitStandartShootingSystem(this);        
    }
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UnitFactory
{
    public static UnitFactory Instance;

    private List<IUnit> units = new List<IUnit>();

    public List<IUnit> Units { get => units; }

    public UnitFactory()
    {
        Instance = this;
    }
    
    public void CreateUnit(UnitStartPosition unitStartPosition)
    {
        switch (unitStartPosition.UnitType)
        {
            case EnumUnitType.Player:
                CreatePlayer(unitStartPosition);
                break;
            case EnumUnitType.Monster:
                CreateMonster(unitStartPosition);
                break;           
        }       
    }
    private void CreatePlayer(UnitStartPosition unitStartPosition)
    {
        Player p = new Player(unitStartPosition, UnitPrefabsHadler.Instance.StandartPlayerView);
        Units.Add(p);
    }

    private void CreateMonster(UnitStartPosition unitStartPosition)
    {
        switch (unitStartPosition.MovementType)
        {
            case EnumUnitMovementType.Walk:
                CreateWalkMonster(unitStartPosition);
                break;
            case EnumUnitMovementType.Run:
                CreateRunMonster(unitStartPosition);
                break;
            case EnumUnitMovementType.Fly:
                CreateFlyMonster(unitStartPosition);
                break;
        }
    }
    private void CreateWalkMonster(UnitStartPosition unitStartPosition)
    {
        WalkMonster w = new WalkMonster(unitStartPosition, UnitPrefabsHadler.Instance.WalkMonsterView);
        Units.Add(w);
    }
    private void CreateRunMonster(UnitStartPosition unitStartPosition)
    {
        RunMonster r = new RunMonster(unitStartPosition, UnitPrefabsHadler.Instance.RunMonsterView);
        Units.Add(r);
    }
    private void CreateFlyMonster(UnitStartPosition unitStartPosition)
    {
        FlyMonster f = new FlyMonster(unitStartPosition, UnitPrefabsHadler.Instance.FlyMonsterView);
        Units.Add(f);
    }

    public void StartUnitsBehaviour()
    {
        foreach(AbstractUnit u in Units)
        {
            if(u.UnitType == EnumUnitType.Monster)
            {
                u.UnitLogicSystem.Start();
            }            
        }
    }

    public IUnit GetPlayer()
    {
        foreach(IUnit u in Units)
        {
            if(u.UnitType == EnumUnitType.Player)
            {
                return u;
            }
        }
        throw new Exception("Player not found.");
    }

    public Vector3 GetUnitPos(IUnit unit)
    {
        foreach(IUnit u in Units)
        {
            if(u == unit)
            {
                return unit.UnitView.transform.position;
            }
        }
        throw new Exception("Unit pos not found.");
    }
    public IUnit GetNearEnemy(IUnit unit)
    {
        foreach(IUnit u in Units)
        {
            if(unit.UnitType == EnumUnitType.Player)
            {
               return GetNearMonster(unit);
            }
            else
            {
                return GetNearPlayer(unit);
            }
        }
        throw new Exception("Target not found.");
    }
    private IUnit GetNearMonster(IUnit unit)
    {
        float distance = 0;
        IUnit target = null;

        foreach (IUnit u in Units)
        {
            if(u.UnitType == EnumUnitType.Monster && Vector3.Distance(unit.UnitView.transform.position, u.UnitView.transform.position) > distance)
            {
                distance = Vector3.Distance(unit.UnitView.transform.position, u.UnitView.transform.position);
                target = u;
            }
        }
        return target;        
    }
    private IUnit GetNearPlayer(IUnit unit)
    {
        float distance = 0;
        IUnit target = null;

        foreach (IUnit u in Units)
        {
            if (u.UnitType == EnumUnitType.Player && Vector3.Distance(unit.UnitView.transform.position, u.UnitView.transform.position) > distance)
            {
                distance = Vector3.Distance(unit.UnitView.transform.position, u.UnitView.transform.position);
                target = u;
            }
        }
        return target;
    }
}

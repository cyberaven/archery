﻿using UnityEngine;
using System.Collections;

public class UnitStartPosition : MonoBehaviour
{
    [SerializeField] private EnumUnitType unitType;
    [SerializeField] private EnumUnitMovementType movementType;   

    public EnumUnitType UnitType { get => unitType; }
    public EnumUnitMovementType MovementType { get => movementType; }
}

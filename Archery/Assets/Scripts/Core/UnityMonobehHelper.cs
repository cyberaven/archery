﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class UnityMonobehHelper : MonoBehaviour
{
    public static UnityMonobehHelper Instance;

    [SerializeField] private UnityTimer UnityTimer;
    private List<UnityTimer> UnityTimers = new List<UnityTimer>();

    private List<Action> FixUpdateList = new List<Action>();

    public delegate void PlayerPressWDel();
    public static event PlayerPressWDel PlayerPressWEvent;

    public delegate void PlayerPressSDel();
    public static event PlayerPressSDel PlayerPressSEvent;

    public delegate void PlayerPressDDel();
    public static event PlayerPressDDel PlayerPressDEvent;

    public delegate void PlayerPressADel();
    public static event PlayerPressADel PlayerPressAEvent;

    public delegate void PlayerUPBtnDel();
    public static event PlayerUPBtnDel PlayerUPBtnEvent;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        PlayerController();
    }
    private void FixedUpdate()
    {
        RunFixUpdateList();
    }

    public void CreateNewFixUpdate(Action a)
    {
        CheckFixUpdateList(a);
        FixUpdateList.Add(a);        
    }
    public void StopFixUpdate(Action a)
    {
        FixUpdateList.Remove(a);
    }
    private void CheckFixUpdateList(Action a)
    {
        for (int i = 0; i < FixUpdateList.Count; i++)
        {
            if(FixUpdateList[i] == a)
            {
                StopFixUpdate(a);
            }
        }
    }
    private void RunFixUpdateList()
    {
        for (int i = 0; i < FixUpdateList.Count; i++)
        {
            FixUpdateList[i]();
        }
    }

    public void CreateNewTimer(Action a, float sec)
    {
        StopTimer(a);
     
        UnityTimer unityTimer = Instantiate(UnityTimer, transform);       
        unityTimer.Method = a;
        unityTimer.MethodCooldown = sec;
        unityTimer.Go();

        UnityTimers.Add(unityTimer);
    }   
    public void StopTimer(Action a)
    {
        for (int i = 0; i < UnityTimers.Count; i++)
        {
            if (UnityTimers[i].Method == a)
            {
                UnityTimers[i].Stop();
                UnityTimers.Remove(UnityTimers[i]);
            }
        }
    }

    private void PlayerController()
    {   
        if (Input.GetKey("w"))
        {
            if(PlayerPressWEvent != null)
            {
                PlayerPressWEvent();
            }
        }    
        if(Input.GetKeyUp("w"))
        {
            if(PlayerUPBtnEvent != null)
            {
                PlayerUPBtnEvent();
            }
        }
        if(Input.GetKey("s"))
        {
            if (PlayerPressSEvent != null)
            {
                PlayerPressSEvent();
            }
        }
        if (Input.GetKeyUp("s"))
        {
            if (PlayerUPBtnEvent != null)
            {
                PlayerUPBtnEvent();
            }
        }
        if (Input.GetKey("a"))
        {
            if (PlayerPressAEvent != null)
            {
                PlayerPressAEvent();
            }
        }
        if (Input.GetKeyUp("a"))
        {
            if (PlayerUPBtnEvent != null)
            {
                PlayerUPBtnEvent();
            }
        }
        if (Input.GetKey("d"))
        {
            if (PlayerPressDEvent != null)
            {
                PlayerPressDEvent();
            }
        }
        if (Input.GetKeyUp("d"))
        {
            if (PlayerUPBtnEvent != null)
            {
                PlayerUPBtnEvent();
            }
        }
    }
}

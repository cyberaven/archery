﻿using UnityEngine;
using System;
using System.Collections;

public class UnityTimer : MonoBehaviour
{   
    public Action Method { get; set; }
    public float MethodCooldown { get; set; }    
    
    public void Go()
    {
        StartCoroutine(Timer());        
    }
    public void Stop()
    {
        StopCoroutine(Timer());
        Destroy(gameObject);        
    }
    IEnumerator Timer()
    {
        while (true)
        {            
            Method();
            yield return new WaitForSeconds(MethodCooldown);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameLogic
{
    private LevelMananager LevelMananager;
    private UnitFactory UnitFactory;   

    public GameLogic()
    {
        LevelMananager = new LevelMananager();
        UnitFactory = new UnitFactory();

        SelectLevelButton.PlayerSelectLevelEvent += PlayerSelectLevel;
    }    

    private void PlayerSelectLevel(LevelView levelView)
    {
        LevelView l = CreateLevel(levelView);
        CreateUnits(l);
        UnitFactory.StartUnitsBehaviour();
    }

    private LevelView CreateLevel(LevelView levelView)
    {
        LevelView level = LevelMananager.LoadLevel(levelView);
        return level;
        
    }
    private void CreateUnits(LevelView levelView)
    {
        foreach (UnitStartPosition unitStartPosition in levelView.UnitStartPositions)
        {
            UnitFactory.CreateUnit(unitStartPosition);
        }
    }    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Добрый Вечер.

Данный код надо дорабатывать. Архитектура для текущей задачи меня устраивает, 
как обычно до поры до времени, но логические связи и некоторые реализации нет. 
Например, движение и поворот надо выносить в разные состояния объекта и инкапсулировать в свое конкретное поведение. 
Поиск пути фактически отсутствует и игровые объекты движутся рандомно, что частенько их натыкает то на друг друга, 
то на стены. Надо вводить проверку рейкастом своего пути и уклонения, что тоже наверняка надо выносить отдельно, 
а не запихивать как сейчас в один класс Системы Движения. Систему Жизни, не успел. Сделал бы так, 
создал интерфейс IHealthSystem, создал в абстрактном классе Unit поле для него, реализовал класс HealthSystem, 
который ведет подсчет кол-ва жизни. В HealthSystem прокидывал события от конкретного UnitView при взаимодействии с коллайдером. 
Все хар-ки и поля для настройки объектов геймдизом хотел вынести в UnitView и 
после его создания, конкретный юнит расфасовал бы данные по нужным системам. 
Что больше всего отняло времени: сделал счетчик стандартными c# средствами, он многопоточный и Unity не хотела 
нормально работать с кодом после создания счетчика. Потратил время на изучения вопроса. 
Остатки не правильного решения до сих пор покоятся в статичном классе RandomForManyThreads. 
Подумал, что смогу распараллелить логику, не смог. Как итог, и надеюсь верное решение, появился класс UnityMonobehHelper, 
в котором запускаются корутины и крутятся методы в FixedUpdate.

 * */

public class Starter : MonoBehaviour
{
    [SerializeField] private UnityMonobehHelper UnityMonobehHelper;
    [SerializeField] private GameUI GameUIPrefab;
    [SerializeField] private UnitPrefabsHadler UnitPrefabsHadler;
    [SerializeField] private BulletPrefabsHandler BulletPrefabsHandler;

    private void Awake()
    {
        Init();
    }
    private void Init()
    {
        UnityMonobehHelper unityMonobehHelper = Instantiate(UnityMonobehHelper);

        UnitPrefabsHadler unitPrefabsHadler = Instantiate(UnitPrefabsHadler);
        BulletPrefabsHandler bulletPrefabsHandler = Instantiate(BulletPrefabsHandler);

        GameUI gameUI = Instantiate(GameUIPrefab);

        GameLogic gameLogic = new GameLogic();                
    }
}

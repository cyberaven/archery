﻿public interface IObservable
{   
    void AddObserver(IObserver o);
    void DelObserver(IObserver o);
    void NotifyObservers();    
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelView : MonoBehaviour
{
    [SerializeField] private List<UnitStartPosition> unitStartPositions = new List<UnitStartPosition>();
    public List<UnitStartPosition> UnitStartPositions { get => unitStartPositions; }

    private void Start()
    {
        HideStartPositions();
    }

    public void HideStartPositions()
    {
        foreach(UnitStartPosition unitStartPosition in UnitStartPositions)
        {
            unitStartPosition.gameObject.SetActive(false);
        }
    }
}

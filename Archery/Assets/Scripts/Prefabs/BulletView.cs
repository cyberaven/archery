﻿using UnityEngine;
using System.Collections;

public class BulletView : MonoBehaviour
{   
    public Vector3 Target { get; set; }
    private float MoveSpeed = 0.001f;
    private bool MoveEnable = false;
    private int LiveTime = 5;

    float MoveProgress = 0f;
   
    private void FixedUpdate()
    {
        Move();
    }

    public void Go()
    {
        MoveEnable = true;
        StartCoroutine(DestroySelf());
    }
    private void Move()
    {
        if (MoveEnable == true)
        {
            transform.position = Vector3.Lerp(transform.position, Target, MoveProgress);
            MoveProgress += MoveSpeed;            
        }        
    }   
    IEnumerator DestroySelf()
    {
        yield return new WaitForSeconds(LiveTime);
        Destroy(gameObject);
    }
}
